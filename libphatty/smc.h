/*
 * SMC: Synchronous Midi Communication interface
 */

#pragma once
#include <alsa/asoundlib.h>
#include <stdint.h>

int smc_open(const char *client_name);
void smc_close();
snd_seq_event_t *smc_next_input_event(uint64_t *timeout);
void smc_clear_input_events();
void smc_put_output_event(snd_seq_event_t *ev);
void smc_put_output_sysex(const uint8_t *dataptr, unsigned datalen);
void smc_put_output_noteon(int ch, int key, int vel);
void smc_put_output_noteoff(int ch, int key, int vel);
void smc_put_output_cc(int ch, int cc, int val);
void smc_put_output_pgmchange(int ch, int pgm);
char *smc_get_input_subscriber();
char *smc_get_output_subscriber();
