prefix=@CMAKE_INSTALL_PREFIX@
exec_prefix=${prefix}
libdir=${exec_prefix}/lib
includedir=${prefix}/include

Name: libphatty
Description: edition library for the Moog Little Phatty synthesizer.
Version: @PROJECT_VERSION@
Requires:
Libs: -L${libdir} -lphatty
Cflags: -I${includedir}
