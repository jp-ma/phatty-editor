#include "util.h"
#include <time.h>
#include <err.h>

uint64_t getproctime() {
#if defined(CLOCK_MONOTONIC_RAW)
  clockid_t clk = CLOCK_MONOTONIC_RAW;
#else
  clockid_t clk = CLOCK_MONOTONIC;
#endif
  struct timespec ts;
  int ret = clock_gettime(clk, &ts);
  if (ret != 0)
    err(1, "cannot read the process clock");
  return TIME_SEC(ts.tv_sec) + ts.tv_nsec;
}

int tpoll(struct pollfd *fds, nfds_t nfds, uint64_t *ptimeout) {
  const uint64_t start = getproctime();
  const uint64_t timeout = *ptimeout;

  struct timespec ts;
  ts.tv_sec = timeout / TIME_SEC(1);
  ts.tv_nsec = timeout - ts.tv_sec * TIME_SEC(1);

  int ret = ppoll(fds, nfds, &ts, NULL);

  const uint64_t interval = getproctime() - start;
  *ptimeout = (interval > timeout) ? 0 : (timeout - interval);

  return ret;
}

void show_bytes(FILE *stream, const uint8_t *data, unsigned len) {
  const uint8_t *base = data;

  while (len > 0) {
    unsigned addr = data - base;
    unsigned thislen = (len > 16) ? 16 : len;

    fprintf(stream, "%08x:", addr);
    unsigned i;
    for (i = 0; i < thislen; ++i) {
      if ((i & 1) == 0) fputc(' ', stream);
      fprintf(stream, "%02x", data[i]);
    }
    for (; i < 16; ++i) {
      if ((i & 1) == 0) fputc(' ', stream);
      fputs("  ", stream);
    }
    fputs("  ", stream);

    for (i = 0; i < thislen; ++i) {
      uint8_t c = data[i];
      fputc((c >= 32 && c <= 126) ? c : '.', stream);
    }
    fputc('\n', stream);

    data += thislen;
    len -= thislen;
  }
}
