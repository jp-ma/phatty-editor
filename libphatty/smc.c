#include "smc.h"
#include "util.h"
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <err.h>

static snd_seq_t *seq_handle = NULL;
static int inportnum = -1, outportnum = -1;
static snd_seq_port_info_t *inport = NULL, *outport = NULL;

int smc_open(const char *client_name) {
  if (seq_handle) {
    fprintf(stderr, "the MIDI interface is already open\n");
    return -1;
  }

  if (snd_seq_open(&seq_handle, "default", SND_SEQ_OPEN_DUPLEX, 0) < 0) {
    return -1;
  }

  snd_seq_set_client_name(seq_handle, client_name);

  inportnum = snd_seq_create_simple_port(seq_handle, client_name,
                                         SND_SEQ_PORT_CAP_WRITE|SND_SEQ_PORT_CAP_SUBS_WRITE,
                                         SND_SEQ_PORT_TYPE_MIDI_GENERIC);
  outportnum = snd_seq_create_simple_port(seq_handle, client_name,
                                          SND_SEQ_PORT_CAP_READ|SND_SEQ_PORT_CAP_SUBS_READ,
                                          SND_SEQ_PORT_TYPE_MIDI_GENERIC);

  if (inportnum < 0 || outportnum < 0) {
    snd_seq_close(seq_handle);
    seq_handle = NULL;
    return -1;
  }

  snd_seq_port_info_malloc(&inport);
  snd_seq_port_info_malloc(&outport);
  snd_seq_get_port_info(seq_handle, inportnum, inport);
  snd_seq_get_port_info(seq_handle, outportnum, outport);

  return 0;
}

void smc_close() {
  if (seq_handle) {
    snd_seq_port_info_free(inport);
    snd_seq_port_info_free(outport);
    snd_seq_close(seq_handle);
    seq_handle = NULL;
  }
}

/* snd_seq_event_t *smc_next_input_event(uint64_t *timeout) { */
/*   int have = 0; */

/*   if (!timeout) { */
/*     have = 1; */
/*   } else { */
/*     const int nfds = snd_seq_poll_descriptors_count(seq_handle, POLLIN); */
/*     struct pollfd pfds[nfds]; */
/*     snd_seq_poll_descriptors(seq_handle, pfds, nfds, POLLIN); */

/*     int ret = tpoll(pfds, nfds, timeout); */
/*     if (ret <= 0) { */
/*       return NULL; */
/*     } */

/*     for (int i = 0; i < nfds && !have; ++i) */
/*       if (pfds[i].revents > 0) */
/*         have = 1; */
/*   } */

/*   snd_seq_event_t *ev = NULL; */
/*   if (have) */
/*     snd_seq_event_input(seq_handle, &ev); */

/*   return ev; */
/* } */

snd_seq_event_t *smc_next_input_event(uint64_t *timeout) {
  assert(timeout);

  int pending = snd_seq_event_input_pending(seq_handle, 1);
  if (pending <= 0) {
    const int nfds = snd_seq_poll_descriptors_count(seq_handle, POLLIN);
    struct pollfd pfds[nfds];
    snd_seq_poll_descriptors(seq_handle, pfds, nfds, POLLIN);
    tpoll(pfds, nfds, timeout);
    pending = snd_seq_event_input_pending(seq_handle, 1);
  }

  snd_seq_event_t *ev = NULL;
  if (pending > 0)
    snd_seq_event_input(seq_handle, &ev);

  return ev;
}

void smc_clear_input_events() {
  snd_seq_event_t *ev;
  do {
    uint64_t timeout = 0;
    ev = smc_next_input_event(&timeout);
    snd_seq_free_event(ev);
  } while (ev);
}

void smc_put_output_event(snd_seq_event_t *ev) {
  snd_seq_ev_set_direct(ev);
  snd_seq_ev_set_subs(ev);
  snd_seq_ev_set_source(ev, outportnum);
  snd_seq_event_output_direct(seq_handle, ev);
}

void smc_put_output_sysex(const uint8_t *dataptr, unsigned datalen) {
  snd_seq_event_t ev;
  snd_seq_ev_clear(&ev);
  snd_seq_ev_set_sysex(&ev, datalen, (void *)dataptr);
  smc_put_output_event(&ev);
}

void smc_put_output_noteon(int ch, int key, int vel) {
  snd_seq_event_t ev;
  snd_seq_ev_clear(&ev);
  snd_seq_ev_set_noteon(&ev, ch, key, vel);
  smc_put_output_event(&ev);
}

void smc_put_output_noteoff(int ch, int key, int vel) {
  snd_seq_event_t ev;
  snd_seq_ev_clear(&ev);
  snd_seq_ev_set_noteoff(&ev, ch, key, vel);
  smc_put_output_event(&ev);
}

void smc_put_output_cc(int ch, int cc, int val) {
  snd_seq_event_t ev;
  snd_seq_ev_clear(&ev);
  snd_seq_ev_set_controller(&ev, ch, cc, val);
  smc_put_output_event(&ev);
}

void smc_put_output_pgmchange(int ch, int pgm) {
  snd_seq_event_t ev;
  snd_seq_ev_clear(&ev);
  snd_seq_ev_set_pgmchange(&ev, ch, pgm);
  smc_put_output_event(&ev);
}

char *smc_get_input_subscriber() {
  snd_seq_query_subscribe_t *query;
  snd_seq_query_subscribe_alloca(&query);

  snd_seq_query_subscribe_set_root(query, snd_seq_port_info_get_addr(inport));
  snd_seq_query_subscribe_set_type(query, SND_SEQ_QUERY_SUBS_WRITE);
  snd_seq_query_subscribe_set_index(query, 0);

  if (snd_seq_query_port_subscribers(seq_handle, query) < 0)
    return NULL;
  const snd_seq_addr_t *srcaddr = snd_seq_query_subscribe_get_addr(query);

  snd_seq_client_info_t *srcclient;
  snd_seq_port_info_t *srcport;
  snd_seq_client_info_alloca(&srcclient);
  snd_seq_port_info_alloca(&srcport);
  if (snd_seq_get_any_client_info(seq_handle, srcaddr->client, srcclient) < 0 ||
      snd_seq_get_any_port_info(seq_handle, srcaddr->client, srcaddr->port, srcport) < 0)
    return NULL;

  const char *client_name = snd_seq_client_info_get_name(srcclient);
  const char *port_name = snd_seq_port_info_get_name(srcport);

  char *result;
  asprintf(&result, "[%d:%d] %s:%s",
           srcaddr->client, srcaddr->port, client_name, port_name);
  return result;
}

char *smc_get_output_subscriber() {
  snd_seq_query_subscribe_t *query;
  snd_seq_query_subscribe_alloca(&query);

  snd_seq_query_subscribe_set_root(query, snd_seq_port_info_get_addr(outport));
  snd_seq_query_subscribe_set_type(query, SND_SEQ_QUERY_SUBS_READ);
  snd_seq_query_subscribe_set_index(query, 0);

  if (snd_seq_query_port_subscribers(seq_handle, query) < 0)
    return NULL;
  const snd_seq_addr_t *dstaddr = snd_seq_query_subscribe_get_addr(query);

  snd_seq_client_info_t *dstclient;
  snd_seq_port_info_t *dstport;
  snd_seq_client_info_alloca(&dstclient);
  snd_seq_port_info_alloca(&dstport);
  if (snd_seq_get_any_client_info(seq_handle, dstaddr->client, dstclient) < 0 ||
      snd_seq_get_any_port_info(seq_handle, dstaddr->client, dstaddr->port, dstport) < 0)
    return NULL;

  const char *client_name = snd_seq_client_info_get_name(dstclient);
  const char *port_name = snd_seq_port_info_get_name(dstport);

  char *result;
  asprintf(&result, "[%d:%d] %s:%s",
           dstaddr->client, dstaddr->port, client_name, port_name);
  return result;
}
