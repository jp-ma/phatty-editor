#include "phatty.h"
#include "smc.h"
#include "util.h"
#include <sys/stat.h>
#include <string.h>

#include "phatty-fmt.x"

static unsigned phatty_channel = 0;
static uint8_t sysex_recvbuf[PHATTY_SYSEX_MAX];
static unsigned sysex_recvlen = 0;

int phatty_open(const char *client_name) {
  int ret = smc_open(client_name);
  return ret;
}

void phatty_close() {
  smc_close();
}

void phatty_set_channel(unsigned channel) {
  assert(channel < 16);
  phatty_channel = channel;
}

static bool phatty_validate_sysex_header(const uint8_t *buf, unsigned len) {
  return len >= 2 && buf[0] == 0xf0 && buf[1] == 0x04;
}

bool phatty_validate_sysex(const uint8_t *buf, unsigned len) {
  return len > 2 && buf[0] == 0xf0 && buf[1] == 0x04 && buf[len - 1] == 0xf7;
}

void phatty_send_raw_sysex(const uint8_t *buf, unsigned len) {
  assert(phatty_validate_sysex(buf, len));
  smc_put_output_sysex(buf, len);
}

void phatty_send_raw_cc7(unsigned cc, unsigned val) {
  assert(cc < 128);
  assert(val < 128);
  smc_put_output_cc(phatty_channel, cc, val);
}

void phatty_send_raw_cc12(unsigned ccmsb, unsigned val) {
  assert(val < 4096);
  phatty_send_raw_cc14(ccmsb, val << 2);
}

void phatty_send_raw_cc14(unsigned ccmsb, unsigned val) {
  assert(ccmsb + 32 < 128);
  assert(val < 16384);
  smc_put_output_cc(phatty_channel, ccmsb, (val >> 7) & 0b1111111);
  smc_put_output_cc(phatty_channel, ccmsb + 32, val & 0b1111111);
}

void phatty_send_pgmchange(unsigned pgm) {
  assert(pgm < 128);
  smc_put_output_pgmchange(phatty_channel, pgm);
}

void phatty_send_panel_request() {
  const uint8_t sysex[] = {
    0xf0, 0x04, 0x05, 0x06, 0x03, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0xf7 };
  phatty_send_raw_sysex(sysex, sizeof(sysex));
}

void phatty_send_preset_request(unsigned preset_num) {
  assert(preset_num < 128);
  const uint8_t sysex[] = {
    0xf0, 0x04, 0x05, 0x06, 0x04, preset_num, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0xf7 };
  phatty_send_raw_sysex(sysex, sizeof(sysex));
}

void phatty_clear_input_messages() {
  smc_clear_input_events();
}

int phatty_receive_message(PhattyMessage *e, unsigned timeout_ms) {
  assert(e);
  e->type = kPhattyMessage_None;

  uint64_t time_left = TIME_MSEC(timeout_ms);

  snd_seq_event_t *msg;
  sysex_recvlen = 0;
  while (e->type == kPhattyMessage_None &&
         (msg = smc_next_input_event(&time_left))) {
    switch (msg->type) {
      case SND_SEQ_EVENT_CONTROLLER: {
        if (msg->data.control.channel == phatty_channel) {
          e->type = kPhattyMessage_CC;
          e->cc.param = msg->data.control.param;
          e->cc.value = msg->data.control.value;
        }
        break;
      }
      case SND_SEQ_EVENT_PGMCHANGE: {
        if (msg->data.control.channel == phatty_channel) {
          e->type = kPhattyMessage_ProgramChange;
          e->pgmchange.value = msg->data.control.value;
        }
        break;
      }
      case SND_SEQ_EVENT_SYSEX: {
        const uint8_t *chunk_data = msg->data.ext.ptr;
        const unsigned chunk_len = msg->data.ext.len;

        const bool is_continued = chunk_data[0] != 0xf0;
        const bool is_complete = chunk_data[chunk_len - 1] == 0xf7;

        if (!is_continued)
          sysex_recvlen = 0;
        if (sysex_recvlen + chunk_len > PHATTY_SYSEX_MAX) {
          sysex_recvlen = 0;
          continue;
        }
        if (!is_continued && !phatty_validate_sysex_header(chunk_data, chunk_len))
          continue;
        if (is_continued && sysex_recvlen == 0)
          continue;

        memcpy(&sysex_recvbuf[sysex_recvlen], chunk_data, chunk_len);
        sysex_recvlen += chunk_len;

        if (is_complete) {
          assert(phatty_validate_sysex(sysex_recvbuf, sysex_recvlen));
          e->type = kPhattyMessage_Sysex;
          e->sysex.data = sysex_recvbuf;
          e->sysex.len = sysex_recvlen;
          sysex_recvlen = 0;
        }
        break;
      }
    }
    snd_seq_free_event(msg);
  }

  return e->type;
}

int phatty_message_get_parameters(const PhattyMessage *e, PhattyParameters *p) {
  if (e->type != kPhattyMessage_Sysex) {
    return -1;
  }

  const uint8_t *data = e->sysex.data;
  unsigned len = e->sysex.len;

  if (len < 172 || !phatty_validate_sysex(data, len)) {
    return -1;
  }

  bool is_panel = data[2] == 0x05 && data[3] == 0x04 && data[4] == 0x03 && data[5] == 0x02;
  bool is_preset = data[2] == 0x05 && data[3] == 0x05 && data[4] == 0x03;
  if (!is_panel && !is_preset) {
    return -1;
  }

  const unsigned offset = len - 172;
  data += offset;
  len = 172;

  PHATTY_READ_NAME(data, p);
  for (unsigned i = 0; i < 13; ++i)
    p->name[i] = phatty_code_char(p->name[i]);
  p->name[13] = '\0';
  PHATTY_READ_PRESET(data, p);

  return 0;
}

void phatty_apply_cc7(PhattyParameters *p, unsigned cc, unsigned val) {
  assert(val < 128);

  switch (cc) {

#define CASE_12BIT(caseval, member)                            \
    case kPhattyCC_##caseval:                                  \
      p->member = (p->member & 0b11111) | (val << 5);          \
      break;                                                   \
    case kPhattyCC_##caseval + 32:                             \
      p->member = (p->member & 0b111111100000) | (val >> 2);   \
      break;

    CASE_12BIT(ModRate, mod_rate);
    CASE_12BIT(ModAmount, mod_amount);
    CASE_12BIT(Osc1Wave, osc1_wave);
    CASE_12BIT(Osc1Level, osc1_level);
    CASE_12BIT(OscGlide, osc_glide);
    CASE_12BIT(Osc2Freq, osc2_freq);
    CASE_12BIT(Osc2Wave, osc2_wave);
    CASE_12BIT(Osc2Level, osc2_level);
    CASE_12BIT(FltCutoff, flt_cutoff);
    CASE_12BIT(FltRes, flt_reso);
    CASE_12BIT(FltKb, flt_kb);
    CASE_12BIT(FltEg, flt_eg);
    CASE_12BIT(FltOl, flt_ol);
    CASE_12BIT(EgVolA, vol_A);
    CASE_12BIT(EgVolD, vol_D);
    CASE_12BIT(EgVolS, vol_S);
    CASE_12BIT(EgVolR, vol_R);
    CASE_12BIT(EgFltA, flt_A);
    CASE_12BIT(EgFltD, flt_D);
    CASE_12BIT(EgFltS, flt_S);
    CASE_12BIT(EgFltR, flt_R);

    case kPhattyCC_Glide:
      p->glide_enable = val != 0;
      break;
    case kPhattyCC_ModSrc:
      switch (val) {
        case 0: default: p->mod_src = kPhattyModSrc_Triangle; break;
        case 16: p->mod_src = kPhattyModSrc_Square; break;
        case 32: p->mod_src = kPhattyModSrc_Sawtooth; break;
        case 48: p->mod_src = kPhattyModSrc_Ramp; break;
        case 64: p->mod_src = kPhattyModSrc_FltEnv; break;
        case 80: p->mod_src = kPhattyModSrc_Osc2Env; break;
      }
      break;
    case kPhattyCC_ModDst:
      switch (val) {
        case 0: default: p->mod_dst = kPhattyModDst_Pitch; break;
        case 16: p->mod_dst = kPhattyModDst_Filter; break;
        case 32: p->mod_dst = kPhattyModDst_Wave; break;
        case 48: p->mod_dst = kPhattyModDst_Osc2; break;
      }
      break;
    case kPhattyCC_Osc1Oct:
      p->osc1_oct = (val < 16 || val > 64) ? 0 : (val / 16 - 1);
      break;
    case kPhattyCC_OscSync:
      p->osc_sync = val != 0;
      break;
    case kPhattyCC_Osc2Oct:
      p->osc2_oct = (val < 16 || val > 64) ? 0 : (val / 16 - 1);
      break;

    case kPhattyCC_FilterPoles:
      p->flt_poles = (val > 96) ? 0 : (val / 32);
      break;
    case kPhattyCC_EgRelease:
      p->eg_release = val != 0;
      break;
    case kPhattyCC_GateMode:
      switch (val) {
        case 0: default: p->gate_mode = kPhattyGateMode_LegOn; break;
        case 43: p->gate_mode = kPhattyGateMode_LegOff; break;
        case 86: p->gate_mode = kPhattyGateMode_LegReset; break;
      }
      break;
    case kPhattyCC_GlideMode:
      p->glide_mode = (val != 0) ? kPhattyGlideMode_On : kPhattyGlideMode_Legato;
      break;
    case kPhattyCC_LfoKbTrig:
      switch (val) {
        case 0: default: p->lfo_kb_trig = kPhattyLfoKbTrig_Off; break;
        case 43: p->lfo_kb_trig = kPhattyLfoKbTrig_On; break;
        case 86: p->lfo_kb_trig = kPhattyLfoKbTrig_Auto; break;
      }
      break;
    case kPhattyCC_FiltSens:
      p->flt_sens = (val < 12 || val > 117) ? 0 : ((val-12) / 7 + 1);
      break;
    case kPhattyCC_VolSens:
      p->vol_sens = (val > 120) ? 0 : (val / 8);
      break;
    case kPhattyCC_ModSrc5:
      switch (val) {
        case 0: default: p->mod_src_5 = kPhattyModSrc5_Filt; break;
        case 64: p->mod_src_5 = kPhattyModSrc5_S_H; break;
      }
      break;
    case kPhattyCC_ModSrc6:
      switch (val) {
        case 0: default: p->mod_src_6 = kPhattyModSrc6_Osc2; break;
        case 64: p->mod_src_6 = kPhattyModSrc6_Noise; break;
      }
      break;
    case kPhattyCC_ModDst2:
      switch (val) {
        case 0: default: p->mod_dst_2 = kPhattyModDst2_Off; break;
        case 25: p->mod_dst_2 = kPhattyModDst2_Pitch; break;
        case 50: p->mod_dst_2 = kPhattyModDst2_Filt; break;
        case 75: p->mod_dst_2 = kPhattyModDst2_Wave; break;
        case 100: p->mod_dst_2 = kPhattyModDst2_Osc2; break;
      }
      break;
    case kPhattyCC_PwUp:
      p->pw_up = (val > 96) ? 0 : (val / 6);
      break;
    case kPhattyCC_PwDown:
      p->pw_dn = (val > 96) ? 0 : (val / 6);
      break;
    case kPhattyCC_KbPrior:
      switch (val) {
        case 0: default: p->kb_prior = kPhattyKbPrior_Glob; break;
        case 32: p->kb_prior = kPhattyKbPrior_Low; break;
        case 64: p->kb_prior = kPhattyKbPrior_High; break;
        case 96: p->kb_prior = kPhattyKbPrior_Last; break;
      }
      break;
    case kPhattyCC_TuningScale:
      p->tun_scale = (val < 32) ? val : 32;
      break;
      //
    case kPhattyCC_ArpClockSrc:
      switch (val) {
        case 0: default: p->arp_clk_src = kPhattyArpClockSrc_Int; break;
        case 43: p->arp_clk_src = kPhattyArpClockSrc_Midi; break;
        case 86: p->arp_clk_src = kPhattyArpClockSrc_Global; break;
      }
      break;
    case kPhattyCC_ArpClockDiv:
      p->arp_clk_div = (val == 127) ? kPhattyArpClockDiv_Global : (val / 6);
      break;
    case kPhattyCC_ArpGateLen:
      switch (val) {
        case 0: default: p->arp_gate_len = kPhattyArpGateLen_50; break;
        case 32: p->arp_gate_len = kPhattyArpGateLen_100; break;
        case 64: p->arp_gate_len = kPhattyArpGateLen_GT_100; break;
        case 96: p->arp_gate_len = kPhattyArpGateLen_Global; break;
      }
      break;
    case kPhattyCC_ArpOctaves:
      switch (val) {
        case 0:   /* -3 */ default: p->arp_octaves = 3; break;
        case 19:  /* -2 */ p->arp_octaves = 4; break;
        case 38:  /* -1 */ p->arp_octaves = 5; break;
        case 57:  /*  0 */ p->arp_octaves = 6; break;
        case 71:  /* +1 */ p->arp_octaves = 0; break;
        case 90:  /* +2 */ p->arp_octaves = 1; break;
        case 109: /* +3 */ p->arp_octaves = 2; break;
      }
      break;
    case kPhattyCC_ArpPattern:
      switch (val) {
        case 0: default: p->arp_pattern = kPhattyArpPattern_Up; break;
        case 43: p->arp_pattern = kPhattyArpPattern_Down; break;
        case 86: p->arp_pattern = kPhattyArpPattern_Order; break;
      }
      break;
    case kPhattyCC_ArpMode:
      switch (val) {
        case 0: default: p->arp_mode = kPhattyArpMode_Loop; break;
        case 43: p->arp_mode = kPhattyArpMode_BackForth; break;
        case 86: p->arp_mode = kPhattyArpMode_Once; break;
      }
      break;

    default:
      break;

#undef CASE_12BIT
  }
}

int phatty_edit_with_parameters(PhattyMessage *e, const PhattyParameters *p) {
  uint8_t *b = e->sysex.data;
  const unsigned len = e->sysex.len;

  if (e->type != kPhattyMessage_Sysex || len < 193)
    return -1;
  b += len - 172;

  struct { char name[13]; } encoded_name;
  for (unsigned i = 0; i < 13; ++i)
    encoded_name.name[i] = phatty_char_code(p->name[i]);
  PHATTY_WRITE_NAME(b, &encoded_name);
  PHATTY_WRITE_PRESET(b, p);

  return 0;
}

PhattyMessage *phatty_message_dup(const PhattyMessage *e) {
  if (!e)
    return NULL;
  PhattyMessage *r;
  switch (e->type) {
    case kPhattyMessage_Sysex:
      r = malloc(sizeof(PhattyMessage) + PHATTY_SYSEX_MAX);
      r->sysex.data = (uint8_t *)&r[1];
      r->sysex.len = e->sysex.len;
      memcpy(r->sysex.data, e->sysex.data, r->sysex.len);
      break;
    default:
      r = malloc(sizeof(PhattyMessage));
      memcpy(r, e, sizeof(PhattyMessage));
  }
  r->type = e->type;
  return r;
}

char phatty_code_char(uint8_t code) {
  const unsigned phatty_numchars = strlen(PHATTY_CHARSET);
  return (code < phatty_numchars) ? PHATTY_CHARSET[code] : ' ';
}

uint8_t phatty_char_code(char ch) {
  const char *pos = strchr(PHATTY_CHARSET, ch);
  return pos ? (pos - PHATTY_CHARSET) : 0;
}

void phatty_describe_parameters(const PhattyParameters *p, FILE *stream) {
  fprintf(stream, "Preset num=%u\n", p->preset_num);
  fprintf(stream, "Name=%s\n", p->name);
  fprintf(stream, "Envelope Volume A=%u D=%u S=%u R=%u\n", p->vol_A, p->vol_D, p->vol_S, p->vol_R);
  fprintf(stream, "Envelope Filter A=%u D=%u S=%u R=%u\n", p->flt_A, p->flt_D, p->flt_S, p->flt_R);
  fprintf(stream, "Oscillator 1 oct=%u wave=%u level=%u\n", p->osc1_oct, p->osc1_wave, p->osc1_level);
  fprintf(stream, "Oscillator 2 oct=%u wave=%u level=%u freq=%u\n", p->osc2_oct, p->osc2_wave, p->osc2_level, p->osc2_freq);
  fprintf(stream, "Oscillator glide=%u sync=%s\n", p->osc_glide, p->osc_sync ? "true" : "false");
  fprintf(stream, "Filter cutoff=%u reso=%u kb=%u eg=%u ol=%u\n", p->flt_cutoff, p->flt_reso, p->flt_kb, p->flt_eg, p->flt_ol);
  fprintf(stream, "Modulation rate=%u amount=%u source=%s destination=%s\n",
          p->mod_rate, p->mod_amount,
          phatty_name_mod_src(p->mod_src),
          phatty_name_mod_dst(p->mod_dst));
  fprintf(stream, "Glide enabled=%s\n", p->glide_enable ? "true" : "false");
  fprintf(stream, "Filter poles=%u\n", p->flt_poles + 1);
  fprintf(stream, "EG release=%s\n", p->eg_release ? "true" : "false");
  fprintf(stream, "Gate mode=%s\n", phatty_name_gate_mode(p->gate_mode));
  fprintf(stream, "Glide mode=%s\n", phatty_name_glide_mode(p->glide_mode));
  fprintf(stream, "LFO kb trig=%s\n", phatty_name_lfo_kb_trig(p->lfo_kb_trig));
  fprintf(stream, "Filter sens=%d\n", (int)p->flt_sens - 8);
  fprintf(stream, "Vol sens=%u\n", p->vol_sens);
  fprintf(stream, "Mod src5=%s src6=%s dst2=%s\n",
          phatty_name_mod_src5(p->mod_src_5),
          phatty_name_mod_src6(p->mod_src_6),
          phatty_name_mod_dst2(p->mod_dst_2));

  static const int pw_values[8] = {0,2,3,4,5,7,12};
  fprintf(stream, "Pitch wheel up=%+d down=%+d\n",
          +pw_values[p->pw_up & 0b111],
          -pw_values[p->pw_dn & 0b111]);

  fprintf(stream, "Kb prior=%s\n", phatty_name_kb_prior(p->kb_prior));
  if (p->tun_scale < 32)
    fprintf(stream, "Tuning scale=%u\n", p->tun_scale);
  else
    fprintf(stream, "Tuning scale=GL\n");

  fputs("Pot mapping", stream);
  if (p->pot_mod_cc == 0)
    fputs(" mod=Off", stream);
  else
    fprintf(stream, " mod=%u %s", p->pot_mod_cc - 1, phatty_name_pot_src(p->pot_mod_src));
  if (p->pot_osc_cc == 0)
    fputs(" osc=Off", stream);
  else
    fprintf(stream, " osc=%u %s", p->pot_osc_cc - 1, phatty_name_pot_src(p->pot_osc_src));
  if (p->pot_flt_cc == 0)
    fputs(" flt=Off", stream);
  else
    fprintf(stream, " flt=%u %s", p->pot_flt_cc - 1, phatty_name_pot_src(p->pot_flt_src));
  if (p->pot_egr_cc == 0)
    fputs(" egr=Off", stream);
  else
    fprintf(stream, " egr=%u %s", p->pot_egr_cc - 1, phatty_name_pot_src(p->pot_egr_src));
  fputc('\n', stream);

  static const int arp_octave_values[8] = {1,2,3,-3,-2,-1,0};
  fprintf(stream, "Arpeggiator clock_source=%s clock_div=%s gate_len=%s octaves=%d pattern=%s mode=%s\n",
          phatty_name_arp_clock_src(p->arp_clk_src),
          phatty_name_arp_clock_div(p->arp_clk_div),
          phatty_name_arp_gate_len(p->arp_gate_len),
          arp_octave_values[p->arp_octaves & 0b111],
          phatty_name_arp_pattern(p->arp_pattern),
          phatty_name_arp_mode(p->arp_mode));

#ifndef PHATTY_BROKEN_SELECTION_PARAMETER
  fprintf(stream, "Selection mod=%s osc=%s flt=%s eg=%s\n",
          phatty_name_selection_mod(p->sel_mod),
          phatty_name_selection_osc(p->sel_osc),
          phatty_name_selection_flt(p->sel_flt),
          phatty_name_selection_eg(p->sel_eg));
#endif
}

const char *phatty_name_mod_src(PhattyModSrc val) {
  switch (val) {
    case kPhattyModSrc_Triangle: return "Triangle";
    case kPhattyModSrc_Square: return "Square";
    case kPhattyModSrc_Sawtooth: return "Sawtooth";
    case kPhattyModSrc_Ramp: return "Ramp";
    case kPhattyModSrc_FltEnv: return "FltEnv";
    case kPhattyModSrc_Osc2Env: return "Osc2Env";
    default: return NULL;
  }
}

const char *phatty_name_mod_dst(PhattyModDst val) {
  switch (val) {
    case kPhattyModDst_Pitch: return "Pitch";
    case kPhattyModDst_Filter: return "Filter";
    case kPhattyModDst_Wave: return "Wave";
    case kPhattyModDst_Osc2: return "Osc2";
    default: return NULL;
  }
}

const char *phatty_name_selection_mod(PhattySelectionMod val) {
  switch (val) {
    case kPhattySelectionMod_Rate: return "Rate";
    case kPhattySelectionMod_Amount: return "Amount";
    default: return NULL;
  }
}

const char *phatty_name_selection_osc(PhattySelectionOsc val) {
  switch (val) {
    case kPhattySelectionOsc_Wave1: return "Wave1";
    case kPhattySelectionOsc_Level1: return "Level1";
    case kPhattySelectionOsc_Glide: return "Glide";
    case kPhattySelectionOsc_Freq2: return "Freq2";
    case kPhattySelectionOsc_Wave2: return "Wave2";
    case kPhattySelectionOsc_Level2: return "Level2";
    default: return NULL;
  }
}

const char *phatty_name_selection_flt(PhattySelectionFlt val) {
  switch (val) {
    case kPhattySelectionFlt_Cutoff: return "Cutoff";
    case kPhattySelectionFlt_Res: return "Res";
    case kPhattySelectionFlt_Kb: return "Kb";
    case kPhattySelectionFlt_Eg: return "Eg";
    case kPhattySelectionFlt_Ol: return "Ol";
    default: return NULL;
  }
}

const char *phatty_name_selection_eg(PhattySelectionEg val) {
  switch (val) {
    case kPhattySelectionEg_VolA: return "VolA";
    case kPhattySelectionEg_VolD: return "VolD";
    case kPhattySelectionEg_VolS: return "VolS";
    case kPhattySelectionEg_VolR: return "VolR";
    case kPhattySelectionEg_FltA: return "FltA";
    case kPhattySelectionEg_FltD: return "FltD";
    case kPhattySelectionEg_FltS: return "FltS";
    case kPhattySelectionEg_FltR: return "FltR";
    default: return NULL;
  }
}

const char *phatty_name_gate_mode(PhattyGateMode val) {
  switch (val) {
    case kPhattyGateMode_LegOff: return "LegOff";
    case kPhattyGateMode_LegOn: return "LegOn";
    case kPhattyGateMode_LegReset: return "LegReset";
    default: return NULL;
  }
}

const char *phatty_name_glide_mode(PhattyGlideMode val) {
  switch (val) {
    case kPhattyGlideMode_On: return "On";
    case kPhattyGlideMode_Legato: return "Legato";
    default: return NULL;
  }
}

const char *phatty_name_lfo_kb_trig(PhattyLfoKbTrig val) {
  switch (val) {
    case kPhattyLfoKbTrig_Off: return "Off";
    case kPhattyLfoKbTrig_On: return "On";
    case kPhattyLfoKbTrig_Auto: return "Auto";
    default: return NULL;
  }
}

const char *phatty_name_mod_src5(PhattyModSrc5 val) {
  switch (val) {
    case kPhattyModSrc5_Filt: return "Filt";
    case kPhattyModSrc5_S_H: return "S-H";
    default: return NULL;
  }
}

const char *phatty_name_mod_src6(PhattyModSrc6 val) {
  switch (val) {
    case kPhattyModSrc6_Osc2: return "Osc2";
    case kPhattyModSrc6_Noise: return "Noise";
    default: return NULL;
  }
}

const char *phatty_name_mod_dst2(PhattyModDst2 val) {
  switch (val) {
    case kPhattyModDst2_Off: return "Off";
    case kPhattyModDst2_Pitch: return "Pitch";
    case kPhattyModDst2_Filt: return "Filt";
    case kPhattyModDst2_Wave: return "Wave";
    case kPhattyModDst2_Osc2: return "Osc2";
    default: return NULL;
  }
}

const char *phatty_name_kb_prior(PhattyKbPrior val) {
  switch (val) {
    case kPhattyKbPrior_Glob: return "Glob";
    case kPhattyKbPrior_Low: return "Low";
    case kPhattyKbPrior_High: return "High";
    case kPhattyKbPrior_Last: return "Last";
    default: return NULL;
  }
}

const char *phatty_name_arp_clock_src(PhattyArpClockSrc val) {
  switch (val) {
    case kPhattyArpClockSrc_Global: return "Global";
    case kPhattyArpClockSrc_Int: return "Int";
    case kPhattyArpClockSrc_Midi: return "Midi";
    default: return NULL;
  }
}

const char *phatty_name_arp_clock_div(PhattyArpClockDiv val) {
  switch (val) {
    case kPhattyArpClockDiv_1_32_T: return "1/32 T";
    case kPhattyArpClockDiv_1_32: return "1/32";
    case kPhattyArpClockDiv_1_16_T: return "1/16T";
    case kPhattyArpClockDiv_1_16: return "1/16";
    case kPhattyArpClockDiv_1_8_T: return "1/8 T";
    case kPhattyArpClockDiv_1_16_DOT: return "1/16 dot";
    case kPhattyArpClockDiv_1_8: return "1/8";
    case kPhattyArpClockDiv_1_4_T: return "1/4 T";
    case kPhattyArpClockDiv_1_8_DOT: return "1/8 dot";
    case kPhattyArpClockDiv_1_4: return "1/4";
    case kPhattyArpClockDiv_1_2_T: return "1/2 T";
    case kPhattyArpClockDiv_1_4_DOT: return "1/4 dot";
    case kPhattyArpClockDiv_1_2: return "1/2";
    case kPhattyArpClockDiv_WH_T: return "WH T";
    case kPhattyArpClockDiv_1_2_DOT: return "1/2 dot";
    case kPhattyArpClockDiv_WH: return "WH";
    case kPhattyArpClockDiv_WH_PLUS_1_4: return "WH+1/4";
    case kPhattyArpClockDiv_WH_PLUS_1_2: return "WH+1/2";
    case kPhattyArpClockDiv_WH_PLUS_1_2_DOT: return "WH+1/2 dot";
    case kPhattyArpClockDiv_2_WH: return "2 WH";
    case kPhattyArpClockDiv_3_WH: return "3 WH";
    case kPhattyArpClockDiv_4_WH: return "4 WH";
    case kPhattyArpClockDiv_Global: return "Global";
    default: return NULL;
  }
}

const char *phatty_name_arp_gate_len(PhattyArpGateLen val) {
  switch (val) {
    case kPhattyArpGateLen_50: return "50%";
    case kPhattyArpGateLen_100: return "100%";
    case kPhattyArpGateLen_GT_100: return ">100%";
    case kPhattyArpGateLen_Global: return "Global";
    default: return NULL;
  }
}

const char *phatty_name_arp_pattern(PhattyArpPattern val) {
  switch (val) {
    case kPhattyArpPattern_Up: return "Up";
    case kPhattyArpPattern_Down: return "Down";
    case kPhattyArpPattern_Order: return "Order";
    default: return NULL;
  }
}

const char *phatty_name_arp_mode(PhattyArpMode val) {
  switch (val) {
    case kPhattyArpMode_Loop: return "Loop";
    case kPhattyArpMode_BackForth: return "Back/Forth";
    case kPhattyArpMode_Once: return "Once";
    default: return NULL;
  }
}

const char *phatty_name_pot_src(PhattyPotSrc val) {
  switch (val) {
    case kPhattyPotSrc_Ext: return "Ext";
    case kPhattyPotSrc_Int: return "Int";
    case kPhattyPotSrc_Both: return "Both";
    default: return NULL;
  }
}
