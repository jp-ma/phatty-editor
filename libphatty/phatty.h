#pragma once
#include "phatty-types.h"

#ifdef __cplusplus
extern "C" {
#endif

#if defined(__GNUC__)
#define PHATTY_API __attribute__((visibility("default")))
#endif

PHATTY_API int phatty_open(const char *client_name);
PHATTY_API void phatty_close();

#define PHATTY_SYSEX_MAX 524288

PHATTY_API void phatty_set_channel(unsigned channel);
PHATTY_API bool phatty_validate_sysex(const uint8_t *buf, unsigned len);
PHATTY_API void phatty_send_raw_sysex(const uint8_t *buf, unsigned len);
PHATTY_API void phatty_send_raw_cc7(unsigned cc, unsigned val);
PHATTY_API void phatty_send_raw_cc12(unsigned ccmsb, unsigned val);
PHATTY_API void phatty_send_raw_cc14(unsigned ccmsb, unsigned val);
PHATTY_API void phatty_send_pgmchange(unsigned pgm);

PHATTY_API void phatty_send_panel_request();
PHATTY_API void phatty_send_preset_request(unsigned preset_num);

PHATTY_API void phatty_clear_input_messages();
PHATTY_API int phatty_receive_message(PhattyMessage *e, unsigned timeout_ms);
PHATTY_API int phatty_message_get_parameters(const PhattyMessage *e, PhattyParameters *p);

PHATTY_API void phatty_apply_cc7(PhattyParameters *p, unsigned cc, unsigned val);
PHATTY_API int phatty_edit_with_parameters(PhattyMessage *e, const PhattyParameters *p);

PHATTY_API PhattyMessage *phatty_message_dup(const PhattyMessage *e);

#define PHATTY_CHARSET                          \
  " "                                           \
  "ABCDEFGHIJKLMNOPQRSTUVWXYZ" "0123456789"     \
  "abcdefghijklmnopqrstuvwxyz" "!#$%&()*?@"

PHATTY_API char phatty_code_char(uint8_t code);
PHATTY_API uint8_t phatty_char_code(char ch);

PHATTY_API void phatty_describe_parameters(const PhattyParameters *p, FILE *stream);

PHATTY_API const char *phatty_name_mod_src(PhattyModSrc val);
PHATTY_API const char *phatty_name_mod_dst(PhattyModDst val);
PHATTY_API const char *phatty_name_selection_mod(PhattySelectionMod val);
PHATTY_API const char *phatty_name_selection_osc(PhattySelectionOsc val);
PHATTY_API const char *phatty_name_selection_flt(PhattySelectionFlt val);
PHATTY_API const char *phatty_name_selection_eg(PhattySelectionEg val);
PHATTY_API const char *phatty_name_gate_mode(PhattyGateMode val);
PHATTY_API const char *phatty_name_glide_mode(PhattyGlideMode val);
PHATTY_API const char *phatty_name_lfo_kb_trig(PhattyLfoKbTrig val);
PHATTY_API const char *phatty_name_mod_src5(PhattyModSrc5 val);
PHATTY_API const char *phatty_name_mod_src6(PhattyModSrc6 val);
PHATTY_API const char *phatty_name_mod_dst2(PhattyModDst2 val);
PHATTY_API const char *phatty_name_kb_prior(PhattyKbPrior val);
PHATTY_API const char *phatty_name_arp_clock_src(PhattyArpClockSrc val);
PHATTY_API const char *phatty_name_arp_clock_div(PhattyArpClockDiv val);
PHATTY_API const char *phatty_name_arp_gate_len(PhattyArpGateLen val);
PHATTY_API const char *phatty_name_arp_pattern(PhattyArpPattern val);
PHATTY_API const char *phatty_name_arp_mode(PhattyArpMode val);
PHATTY_API const char *phatty_name_pot_src(PhattyPotSrc val);

#ifdef __cplusplus
}  // extern "C"
#endif
