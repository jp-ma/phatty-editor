#pragma once
#include <poll.h>
#include <stdint.h>
#include <stdio.h>

#define TIME_NSEC(x) ((uint64_t)(x))
#define TIME_USEC(x) ((uint64_t)(UINT64_C(1000) * (x)))
#define TIME_MSEC(x) ((uint64_t)(UINT64_C(1000000) * (x)))
#define TIME_SEC(x)  ((uint64_t)(UINT64_C(1000000000) * (x)))

uint64_t getproctime();
int tpoll(struct pollfd *fds, nfds_t nfds, uint64_t *ptimeout);

void show_bytes(FILE *stream, const uint8_t *data, unsigned len);
