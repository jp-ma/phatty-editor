#pragma once
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

// TODO fix it, this is set until it is done
#define PHATTY_BROKEN_SELECTION_PARAMETER 1

typedef struct PhattyMessage {
  int type;
  union {
    struct { uint8_t *data; unsigned len; } sysex;
    struct { unsigned param; unsigned value; } cc;
    struct { unsigned value; } pgmchange;
  };
} PhattyMessage;

typedef enum PhattyMessageType {
  kPhattyMessage_None,
  kPhattyMessage_CC,
  kPhattyMessage_ProgramChange,
  kPhattyMessage_Sysex,
} PhattyMessageType;

typedef struct PhattyParameters {
  unsigned preset_num;
  char name[14];
  // Envelope generator
  unsigned vol_A;
  unsigned vol_D;
  unsigned vol_S;
  unsigned vol_R;
  unsigned flt_A;
  unsigned flt_D;
  unsigned flt_S;
  unsigned flt_R;
  // Oscillators
  unsigned osc1_oct;
  unsigned osc1_wave;
  unsigned osc1_level;
  unsigned osc_glide;
  bool osc_sync;
  unsigned osc2_oct;
  unsigned osc2_wave;
  unsigned osc2_level;
  unsigned osc2_freq;
  // Filter
  unsigned flt_cutoff;
  unsigned flt_reso;
  unsigned flt_kb;
  unsigned flt_eg;
  unsigned flt_ol;
  // Modulators
  unsigned mod_rate;
  unsigned mod_amount;
  unsigned mod_src;
  unsigned mod_dst;
  // Glide
  bool glide_enable;
  // Advanced Preset
  unsigned flt_poles;
  bool eg_release;
  unsigned gate_mode;
  unsigned glide_mode;
  unsigned lfo_kb_trig;
  unsigned flt_sens;
  unsigned vol_sens;
  unsigned mod_src_5;
  unsigned mod_src_6;
  unsigned mod_dst_2;
  unsigned pw_up;
  unsigned pw_dn;
  unsigned kb_prior;
  unsigned tun_scale;
  // Pot mapping
  unsigned pot_mod_cc;
  unsigned pot_mod_src;
  unsigned pot_osc_cc;
  unsigned pot_osc_src;
  unsigned pot_flt_cc;
  unsigned pot_flt_src;
  unsigned pot_egr_cc;
  unsigned pot_egr_src;
  // Arpeggiator
  unsigned arp_clk_src;
  unsigned arp_clk_div;
  unsigned arp_gate_len;
  unsigned arp_octaves;
  unsigned arp_pattern;
  unsigned arp_mode;
#ifndef PHATTY_BROKEN_SELECTION_PARAMETER
  // Selection
  unsigned sel_mod;
  unsigned sel_osc;
  unsigned sel_flt;
  unsigned sel_eg;
#endif
} PhattyParameters;

// Control change identifiers
typedef enum PhattyCC {
  kPhattyCC_Glide = 65,
  kPhattyCC_ModRate = 3,
  kPhattyCC_ModAmount = 6,
  kPhattyCC_ModSrc = 68,
  kPhattyCC_ModDst = 69,
  kPhattyCC_Osc1Oct = 74,
  kPhattyCC_Osc1Wave = 9,
  kPhattyCC_Osc1Level = 15,
  kPhattyCC_OscGlide = 5,
  kPhattyCC_OscSync = 77,
  kPhattyCC_Osc2Freq = 10,
  kPhattyCC_Osc2Wave = 11,
  kPhattyCC_Osc2Level = 16,
  kPhattyCC_Osc2Oct = 75,
  kPhattyCC_FltCutoff = 19,
  kPhattyCC_FltRes = 21,
  kPhattyCC_FltKb = 22,
  kPhattyCC_FltEg = 27,
  kPhattyCC_FltOl = 18,
  kPhattyCC_EgVolA = 28,
  kPhattyCC_EgVolD = 29,
  kPhattyCC_EgVolS = 30,
  kPhattyCC_EgVolR = 31,
  kPhattyCC_EgFltA = 23,
  kPhattyCC_EgFltD = 24,
  kPhattyCC_EgFltS = 25,
  kPhattyCC_EgFltR = 26,
  //
  kPhattyCC_FilterPoles = 109,
  kPhattyCC_EgRelease = 88,
  kPhattyCC_GateMode = 112,
  kPhattyCC_GlideMode = 94,
  kPhattyCC_LfoKbTrig = 93,
  kPhattyCC_FiltSens = 110,
  kPhattyCC_VolSens = 92,
  kPhattyCC_ModSrc5 = 104,
  kPhattyCC_ModSrc6 = 105,
  kPhattyCC_ModDst2 = 106,
  kPhattyCC_PwUp = 107,
  kPhattyCC_PwDown = 108,
  kPhattyCC_KbPrior = 111,
  kPhattyCC_TuningScale = 113,
  //
  kPhattyCC_ArpClockSrc = 114,
  kPhattyCC_ArpClockDiv = 115,
  kPhattyCC_ArpGateLen = 95,
  kPhattyCC_ArpOctaves = 116,
  kPhattyCC_ArpPattern = 117,
  kPhattyCC_ArpMode = 118,
} PhattyCC;

// Modulation source
typedef enum PhattyModSrc {
  kPhattyModSrc_Triangle,
  kPhattyModSrc_Square,
  kPhattyModSrc_Sawtooth,
  kPhattyModSrc_Ramp,
  kPhattyModSrc_FltEnv,
  kPhattyModSrc_Osc2Env,
} PhattyModSrc;

// Modulation destination
typedef enum PhattyModDst {
  kPhattyModDst_Pitch,
  kPhattyModDst_Filter,
  kPhattyModDst_Wave,
  kPhattyModDst_Osc2,
} PhattyModDst;

typedef enum PhattySelectionMod {
  kPhattySelectionMod_Rate = 0b1,
  kPhattySelectionMod_Amount = 0b0,
} PhattySelectionMod;

typedef enum PhattySelectionOsc {
  kPhattySelectionOsc_Wave1 = 0b1011,
  kPhattySelectionOsc_Level1 = 0b1010,
  kPhattySelectionOsc_Glide = 0b0100,
  kPhattySelectionOsc_Freq2 = 0b1111,
  kPhattySelectionOsc_Wave2 = 0b1101,
  kPhattySelectionOsc_Level2 = 0b1100,
} PhattySelectionOsc;

typedef enum PhattySelectionFlt {
  kPhattySelectionFlt_Cutoff = 0b011,
  kPhattySelectionFlt_Res = 0b010,
  kPhattySelectionFlt_Kb = 0b001,
  kPhattySelectionFlt_Eg = 0b000,
  kPhattySelectionFlt_Ol = 0b101,
} PhattySelectionFlt;

typedef enum PhattySelectionEg {
  kPhattySelectionEg_VolA = 0b1,
  kPhattySelectionEg_VolD = 0b0,
  kPhattySelectionEg_VolS = 0b11,
  kPhattySelectionEg_VolR = 0b10,
  kPhattySelectionEg_FltA = 0b101,
  kPhattySelectionEg_FltD = 0b100,
  kPhattySelectionEg_FltS = 0b111,
  kPhattySelectionEg_FltR = 0b110,
} PhattySelectionEg;

typedef enum PhattyGateMode {
  kPhattyGateMode_LegOff = 0b1,
  kPhattyGateMode_LegOn = 0b0,
  kPhattyGateMode_LegReset = 0b100,
} PhattyGateMode;

typedef enum PhattyGlideMode {
  kPhattyGlideMode_On = 0b0,
  kPhattyGlideMode_Legato = 0b1,
} PhattyGlideMode;

typedef enum PhattyLfoKbTrig {
  kPhattyLfoKbTrig_Off = 0b0,
  kPhattyLfoKbTrig_On = 0b1,
  kPhattyLfoKbTrig_Auto = 0b10,
} PhattyLfoKbTrig;

typedef enum PhattyModSrc5 {
  kPhattyModSrc5_Filt = 0b0,
  kPhattyModSrc5_S_H = 0b1,
} PhattyModSrc5;

typedef enum PhattyModSrc6 {
  kPhattyModSrc6_Osc2 = 0b0,
  kPhattyModSrc6_Noise = 0b1,
} PhattyModSrc6;

typedef enum PhattyModDst2 {
  kPhattyModDst2_Off = 0b0,
  kPhattyModDst2_Pitch = 0b1,
  kPhattyModDst2_Filt = 0b10,
  kPhattyModDst2_Wave = 0b11,
  kPhattyModDst2_Osc2 = 0b100,
} PhattyModDst2;

typedef enum PhattyKbPrior {
  kPhattyKbPrior_Glob = 0b0,
  kPhattyKbPrior_Low = 0b1,
  kPhattyKbPrior_High = 0b10,
  kPhattyKbPrior_Last = 0b11,
} PhattyKbPrior;

typedef enum PhattyArpClockSrc {
  kPhattyArpClockSrc_Global = 0b101,
  kPhattyArpClockSrc_Int = 0b001,
  kPhattyArpClockSrc_Midi = 0b011,
} PhattyArpClockSrc;

typedef enum PhattyArpClockDiv {
  kPhattyArpClockDiv_1_32_T,
  kPhattyArpClockDiv_1_32,
  kPhattyArpClockDiv_1_16_T,
  kPhattyArpClockDiv_1_16,
  kPhattyArpClockDiv_1_8_T,
  kPhattyArpClockDiv_1_16_DOT,
  kPhattyArpClockDiv_1_8,
  kPhattyArpClockDiv_1_4_T,
  kPhattyArpClockDiv_1_8_DOT,
  kPhattyArpClockDiv_1_4,
  kPhattyArpClockDiv_1_2_T,
  kPhattyArpClockDiv_1_4_DOT,
  kPhattyArpClockDiv_1_2,
  kPhattyArpClockDiv_WH_T,
  kPhattyArpClockDiv_1_2_DOT,
  kPhattyArpClockDiv_WH,
  kPhattyArpClockDiv_WH_PLUS_1_4,
  kPhattyArpClockDiv_WH_PLUS_1_2,
  kPhattyArpClockDiv_WH_PLUS_1_2_DOT,
  kPhattyArpClockDiv_2_WH,
  kPhattyArpClockDiv_3_WH,
  kPhattyArpClockDiv_4_WH,
  kPhattyArpClockDiv_Global,
} PhattyArpClockDiv;

typedef enum PhattyArpGateLen {
  kPhattyArpGateLen_50,
  kPhattyArpGateLen_100,
  kPhattyArpGateLen_GT_100,
  kPhattyArpGateLen_Global,
} PhattyArpGateLen;

typedef enum PhattyArpPattern {
  kPhattyArpPattern_Up,
  kPhattyArpPattern_Down,
  kPhattyArpPattern_Order,
} PhattyArpPattern;

typedef enum PhattyArpMode {
  kPhattyArpMode_Loop = 0b0,
  kPhattyArpMode_BackForth = 0b01,
  kPhattyArpMode_Once = 0b10,
} PhattyArpMode;

typedef enum PhattyPotSrc {
  kPhattyPotSrc_Ext,
  kPhattyPotSrc_Int,
  kPhattyPotSrc_Both,
} PhattyPotSrc;
