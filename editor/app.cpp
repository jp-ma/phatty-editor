#include "app.h"
#include "dispatcher.h"
#include "mainwindow.h"
#include <QtCore/QTimer>
#include <QtCore/QDebug>
#include <phatty.h>
#include <unistd.h>

struct App::Impl {
  Dispatcher *dispatcher = nullptr;
  MainWindow *mainWindow = nullptr;
};
///

App::App(int &argc, char **argv)
    : SingleApplication(argc, argv)
    , P(new Impl) {
}

App::~App() {
}

void App::init() {
  phatty_open("Phatty Editor");
  // need delay, otherwise the Moog ignores the first messages
  sleep(1);

  P->dispatcher = new Dispatcher(this);
  QTimer *dispatchTimer = new QTimer(this);
  dispatchTimer->setInterval(100);
  connect(dispatchTimer, &QTimer::timeout,
          [this]() { P->dispatcher->receive(0); });
  dispatchTimer->start();

  MainWindow *win = new MainWindow;
  P->mainWindow = win;
  win->show();

  connect(this, &SingleApplication::showUp,
          [this]() { P->mainWindow->activateWindow(); });

  win->receiveData();
}

void App::shutdown() {
  phatty_close();
}

Dispatcher *App::dispatcher() const {
  return P->dispatcher;
}
