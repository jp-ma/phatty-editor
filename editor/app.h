#pragma once
#include "single-application.h"
// #include <QtWidgets/QApplication>
#include <memory>

class Dispatcher;

class App: public SingleApplication {
 public:
  App(int &argc, char **argv);
  ~App();
  void init();
  void shutdown();

  Dispatcher *dispatcher() const;

 private:
  struct Impl;
  std::unique_ptr<Impl> P;
};

#define APP() (static_cast<App *>(qApp))
