#include "util.h"

void show_bytes(FILE *stream, const uint8_t *data, unsigned len) {
  const uint8_t *base = data;

  while (len > 0) {
    unsigned addr = data - base;
    unsigned thislen = (len > 16) ? 16 : len;

    fprintf(stream, "%08x:", addr);
    unsigned i;
    for (i = 0; i < thislen; ++i) {
      if ((i & 1) == 0) fputc(' ', stream);
      fprintf(stream, "%02x", data[i]);
    }
    for (; i < 16; ++i) {
      if ((i & 1) == 0) fputc(' ', stream);
      fputs("  ", stream);
    }
    fputs("  ", stream);

    for (i = 0; i < thislen; ++i) {
      uint8_t c = data[i];
      fputc((c >= 32 && c <= 126) ? c : '.', stream);
    }
    fputc('\n', stream);

    data += thislen;
    len -= thislen;
  }
}
