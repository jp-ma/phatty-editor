#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "app.h"
#include "dispatcher.h"
#include "program-name-validator.h"
#include "scope-guard.h"
#include "util.h"
#include <QtCore/QDebug>
#include <QtCore/QResource>
#include <phatty.h>
#include <cassert>

struct MainWindow::Impl {
  Impl();
  ~Impl();

  Ui::MainWindow ui;
  PhattyMessage *currPreset = nullptr;  // the preset of the active program
  PhattyParameters currParams;          // the parameters currently on panel
  void setupControls();
  void setupConnections(MainWindow *self);
  void showParameters();
  void assignInitialData();
  bool assignPreset(const uint8_t *data, unsigned len);
  bool assignPreset(const PhattyMessage *m);
  //
  const QVector<QWidget *> &getCtls();
  const QVector<QAbstractSlider *> &getCtls12bit();
  const QVector<unsigned> &getCCs12bit();
  //
  void midiTest();
  void receiveData();
  void incomingCC(unsigned cc, unsigned val);
  void showMidiTestResult(bool rxOk, bool txOk);
};
///

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , P(new Impl) {
  P->ui.setupUi(this);
  P->assignInitialData();
  P->setupControls();
  P->setupConnections(this);
  P->showParameters();
}

MainWindow::~MainWindow() {
}

///
MainWindow::Impl::Impl() {
}

MainWindow::Impl::~Impl() {
  free(this->currPreset);
}

void MainWindow::Impl::setupControls() {
  ui.txtProgramName->setMaxLength(13);
  ui.txtProgramName->setValidator(new ProgramNameValidator);
  ui.valProgramNumber->setMinimum(0);
  ui.valProgramNumber->setMaximum(99);

  const QVector<QAbstractSlider *> &ctls12Bit = getCtls12bit();
  const QVector<unsigned> &ccs12Bit = getCCs12bit();
  assert(ctls12Bit.size() == ccs12Bit.size());

  for (unsigned i = 0; i < ctls12Bit.size(); ++i) {
    QAbstractSlider *ctl = ctls12Bit[i];
    ctl->setMinimum(0);
    ctl->setMaximum(4095);
    ctl->setTracking(true);
    ctl->setProperty("ccBits", 12);
    ctl->setProperty("ccNumber", ccs12Bit[i]);
  }

  ui.chModSrc->addItem("Triangle", kPhattyModSrc_Triangle);
  ui.chModSrc->addItem("Square", kPhattyModSrc_Square);
  ui.chModSrc->addItem("Sawtooth", kPhattyModSrc_Sawtooth);
  ui.chModSrc->addItem("Ramp", kPhattyModSrc_Ramp);
  ui.chModSrc->addItem("Flt.Env", kPhattyModSrc_FltEnv);
  ui.chModSrc->addItem("Osc2.Env", kPhattyModSrc_Osc2Env);

  ui.chModDst->addItem("Pitch", kPhattyModDst_Pitch);
  ui.chModDst->addItem("Filter", kPhattyModDst_Filter);
  ui.chModDst->addItem("Wave", kPhattyModDst_Wave);
  ui.chModDst->addItem("Osc2", kPhattyModDst_Osc2);

  QComboBox *chOctave[] = { ui.chOsc1Oct, ui.chOsc2Oct };
  for (QComboBox *ctl : chOctave) {
    ctl->addItem("16'", 0);
    ctl->addItem("8'", 1);
    ctl->addItem("4'", 2);
    ctl->addItem("2'", 3);
  }
}

void MainWindow::Impl::setupConnections(MainWindow *self) {
  connect(ui.btnMidiTest, &QAbstractButton::clicked,
          [this]() { this->midiTest(); });
  connect(ui.btnReceiveData, &QAbstractButton::clicked,
          [self]() { self->receiveData(); });

  Dispatcher *dispatcher = APP()->dispatcher();
  // TODO connect dispatcher
  connect(dispatcher, &Dispatcher::ccReceived,
          [this](unsigned cc, unsigned val) { this->incomingCC(cc, val); });
  connect(dispatcher, &Dispatcher::pgmchangeReceived,
          [self](unsigned pgm) { self->receiveData(); });

  for (QAbstractSlider *ctl12Bit : getCtls12bit()) {
    connect(ctl12Bit, &QAbstractSlider::valueChanged,
            [ctl12Bit](int value) {
              // send CC
              phatty_send_raw_cc12(ctl12Bit->property("ccNumber").toUInt(), value);
              // TODO: change value in parameters
            });
    
  }
}

void MainWindow::Impl::showParameters() {
  const PhattyParameters *p = &this->currParams;

  for (QWidget *ctl : getCtls())
    ctl->blockSignals(true);

  ui.valProgramNumber->setValue(p->preset_num - 1);
  ui.txtProgramName->setText(p->name);

  ui.chkGlide->setChecked(p->glide_enable);

  ui.valModRate->setValue(p->mod_rate);
  ui.valModAmount->setValue(p->mod_amount);
  ui.chModSrc->setCurrentIndex(ui.chModSrc->findData(p->mod_src));
  ui.chModDst->setCurrentIndex(ui.chModDst->findData(p->mod_dst));

  ui.valOsc1Wave->setValue(p->osc1_wave);
  ui.valOsc1Level->setValue(p->osc1_level);
  ui.chOsc1Oct->setCurrentIndex(ui.chOsc1Oct->findData(p->osc1_oct));
  ui.valOsc2Wave->setValue(p->osc2_wave);
  ui.valOsc2Level->setValue(p->osc2_level);
  ui.valOsc2Freq->setValue(p->osc2_freq);
  ui.chOsc2Oct->setCurrentIndex(ui.chOsc2Oct->findData(p->osc2_oct));
  ui.valOscGlide->setValue(p->osc_glide);
  ui.chkOscSync->setChecked(p->osc_sync);

  ui.valFltCutoff->setValue(p->flt_cutoff);
  ui.valFltRes->setValue(p->flt_reso);
  ui.valFltKb->setValue(p->flt_kb);
  ui.valFltEg->setValue(p->flt_eg);
  ui.valFltOl->setValue(p->flt_ol);

  ui.valEgVolA->setValue(p->vol_A);
  ui.valEgVolD->setValue(p->vol_D);
  ui.valEgVolS->setValue(p->vol_S);
  ui.valEgVolR->setValue(p->vol_R);
  ui.valEgFltA->setValue(p->flt_A);
  ui.valEgFltD->setValue(p->flt_D);
  ui.valEgFltS->setValue(p->flt_S);
  ui.valEgFltR->setValue(p->flt_R);

  for (QWidget *ctl : getCtls())
    ctl->blockSignals(false);
}

void MainWindow::Impl::assignInitialData() {
  QResource rsc("data/default-preset.syx");
  if (!assignPreset(rsc.data(), rsc.size()))
    assert(0);
}

bool MainWindow::Impl::assignPreset(const uint8_t *data, unsigned len) {
  if (len >= PHATTY_SYSEX_MAX) {
    return false;
  }
  PhattyMessage m;
  m.type = kPhattyMessage_Sysex;
  m.sysex.data = (uint8_t *)data;
  m.sysex.len = len;
  return assignPreset(&m);
}

bool MainWindow::Impl::assignPreset(const PhattyMessage *m) {
  PhattyParameters p;
  if (phatty_message_get_parameters(m, &p) != 0) {
    return false;
  }
  free(this->currPreset);
  this->currPreset = phatty_message_dup(m);
  this->currParams = p;
  return true;
}

const QVector<QWidget *> &MainWindow::Impl::getCtls() {
  static const QVector<QWidget *> ctls = {
    ui.valProgramNumber, ui.txtProgramName,
    ui.chkGlide,
    ui.valModRate, ui.valModAmount, ui.chModSrc, ui.chModDst,
    ui.valOsc1Wave, ui.valOsc1Level, ui.chOsc1Oct,
    ui.valOsc2Wave, ui.valOsc2Level, ui.valOsc2Freq, ui.chOsc2Oct,
    ui.valOscGlide, ui.chkOscSync,
    ui.valFltCutoff, ui.valFltRes, ui.valFltKb, ui.valFltEg, ui.valFltOl,
    ui.valEgVolA, ui.valEgVolD, ui.valEgVolS, ui.valEgVolR,
    ui.valEgFltA, ui.valEgFltD, ui.valEgFltS, ui.valEgFltR,
  };
  return ctls;
}

const QVector<QAbstractSlider *> &MainWindow::Impl::getCtls12bit() {
  static const QVector<QAbstractSlider *> ctls = {
    ui.valModRate, ui.valModAmount,
    ui.valOsc1Wave, ui.valOsc1Level,
    ui.valOscGlide,
    ui.valOsc2Wave, ui.valOsc2Level, ui.valOsc2Freq,
    ui.valFltCutoff, ui.valFltRes, ui.valFltKb, ui.valFltEg, ui.valFltOl,
    ui.valEgVolA, ui.valEgVolD, ui.valEgVolS, ui.valEgVolR,
    ui.valEgFltA, ui.valEgFltD, ui.valEgFltS, ui.valEgFltR,
  };
  return ctls;
}

const QVector<unsigned> &MainWindow::Impl::getCCs12bit() {
  static const QVector<unsigned> ccs = {
    3, 6,
    9, 15,
    5,
    11, 16, 10,
    19, 21, 22, 27, 18,
    28, 29, 30, 31,
    23, 24, 25, 26,
  };
  return ccs;
}

void MainWindow::Impl::midiTest() {
  bool rxOk = false;
  bool txOk = false;

  // phatty_clear_input_messages();
  // phatty_send_panel_request();

  // PhattyParameters p;
  // PhattyMessage m;
  // do {
  //   phatty_receive_message(&m);
  //   if (m.type == kPhattyMessage_Sysex &&
  //       phatty_message_get_parameters(&m, &p) == 0) {
  //     rxOk = true;
  //   }
  // } while (!rxOk &&
  //          m.type != kPhattyMessage_Sysex && m.type != kPhattyMessage_None);

  // if (rxOk) {
  //   // TODO test Tx: send a CC, receive panel and check, restore old CC value
  //   txOk = true;
  // }

  // showMidiTestResult(rxOk, txOk);
}

bool MainWindow::receiveData() {
  Dispatcher *dispatcher = APP()->dispatcher();

  PhattyMessage *panel = nullptr, *preset = nullptr;

  auto sg_cleanup = make_scope_guard(
      [panel, preset]() { free(panel); free(preset); });

  dispatcher->receive(0);
  phatty_send_panel_request();

  panel = dispatcher->receiveUpToNextSysex(500);
  if (!panel) {
    return false;
  }

  PhattyParameters parm;
  if (phatty_message_get_parameters(panel, &parm) != 0) {
    return false;
  }

  // show_bytes(stderr, panel->sysex.data, panel->sysex.len);
  // phatty_describe_parameters(&parm, stderr);

  unsigned preset_num = parm.preset_num - 1;
  if (preset_num > 99) {
    return false;
  }

  dispatcher->receive(0);
  phatty_send_preset_request(preset_num);

  preset = dispatcher->receiveUpToNextSysex(500);
  if (!preset) {
    return false;
  }

  { PhattyParameters parmUnused;
    if (phatty_message_get_parameters(preset, &parmUnused) != 0) {
      return false;
    }
    // phatty_describe_parameters(&parmUnused, stderr);
  }

  free(P->currPreset);
  P->currPreset = preset;
  preset = nullptr;
  P->currParams = parm;

  P->showParameters();
  return true;
}

void MainWindow::Impl::incomingCC(unsigned cc, unsigned val) {
  phatty_apply_cc7(&this->currParams, cc, val);
  this->showParameters();
}

void MainWindow::Impl::showMidiTestResult(bool rxOk, bool txOk) {
  QPalette pal;

  pal = ui.frmRx->palette();
  pal.setColor(ui.frmRx->backgroundRole(), rxOk ? Qt::green : Qt::red);
  ui.frmRx->setAutoFillBackground(true);
  ui.frmRx->setPalette(pal);

  pal = ui.frmTx->palette();
  pal.setColor(ui.frmTx->backgroundRole(), txOk ? Qt::green : Qt::red);
  ui.frmTx->setAutoFillBackground(true);
  ui.frmTx->setPalette(pal);
}
