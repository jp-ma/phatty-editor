#pragma once
#include <stdint.h>
#include <stdio.h>

void show_bytes(FILE *stream, const uint8_t *data, unsigned len);
