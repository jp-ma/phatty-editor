#pragma once
#include <QtGui/QValidator>

class ProgramNameValidator : public QValidator {
 public:
  State validate(QString &input, int &pos) const override;
};
