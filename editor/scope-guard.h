// -*- mode: c++; -*-
#pragma once
#include <utility>

template <typename F>
class scope_guard {
private:
  F f;
public:
  inline scope_guard(scope_guard<F> &&o): f(std::move(o.f)) {}
  inline scope_guard(F &&f): f(std::forward<F>(f)) {}
  inline ~scope_guard() { f(); }
};

template <typename F>
inline scope_guard<F> make_scope_guard(F &&f) {
  return scope_guard<F>(std::forward<F>(f));
}
