#pragma once
#include <QtWidgets/QApplication>

/**
 * @brief The SingleApplication class handles multipe instances of the same
 * Application
 * @see QApplication
 */
class SingleApplication : public QApplication {
  Q_OBJECT;

 public:
  explicit SingleApplication(int &, char *[]);
  virtual ~SingleApplication();

  typedef QApplication super_type;

 signals:
  void showUp();

 private slots:
  void slotConnectionEstablished();

 private:
  struct Private;
  Private *d_ptr;
};
