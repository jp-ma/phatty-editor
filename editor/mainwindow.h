#pragma once
#include <QtWidgets/QMainWindow>
#include <memory>

class MainWindow : public QMainWindow {
  Q_OBJECT;

 public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

  bool receiveData();

 private:
  struct Impl;
  std::unique_ptr<MainWindow::Impl> P;
};
