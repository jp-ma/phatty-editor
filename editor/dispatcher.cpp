#include "dispatcher.h"
#include <phatty.h>
#include <assert.h>

struct Dispatcher::Impl {
  bool do_cc = true;
  bool do_pgmchange = true;
  bool do_sysex = true;
  void emitMessage(Dispatcher *self, const PhattyMessage *msg);
};

Dispatcher::Dispatcher(QObject *parent)
    : QObject(parent)
    , P(new Impl) {
}

Dispatcher::~Dispatcher() {
}

void Dispatcher::receive(unsigned timeout_ms) {
  PhattyMessage m;
  while (phatty_receive_message(&m, timeout_ms) != kPhattyMessage_None) {
    P->emitMessage(this, &m);
  }
}

PhattyMessage *Dispatcher::receiveUpToNextSysex(unsigned timeout_ms) {
  PhattyMessage m;
  while (phatty_receive_message(&m, timeout_ms) != kPhattyMessage_None) {
    P->emitMessage(this, &m);
    if (m.type == kPhattyMessage_Sysex) {
      PhattyMessage *r = phatty_message_dup(&m);
      return r;
    }
  }
  return nullptr;
}

void Dispatcher::Impl::emitMessage(Dispatcher *self, const PhattyMessage *m) {
  switch (m->type) {
    case kPhattyMessage_CC:
      if (this->do_cc)
        emit self->ccReceived(m->cc.param, m->cc.value);
      break;
    case kPhattyMessage_ProgramChange:
      if (this->do_pgmchange)
        emit self->pgmchangeReceived(m->pgmchange.value);
      break;
    case kPhattyMessage_Sysex:
      if (this->do_sysex)
        emit self->sysexReceived(m->sysex.data, m->sysex.len);
      break;
    default:
      assert(0);
  }
}
