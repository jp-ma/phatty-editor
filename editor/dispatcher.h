#pragma once
#include <QtCore/QObject>
#include <memory>

struct PhattyMessage;
///

class Dispatcher : public QObject {
  Q_OBJECT;

 public:
  explicit Dispatcher(QObject *parent = nullptr);
  ~Dispatcher();
  void receive(unsigned timeout_ms);
  PhattyMessage *receiveUpToNextSysex(unsigned timeout_ms);

 signals:
  void ccReceived(unsigned cc, unsigned val);
  void pgmchangeReceived(unsigned pgm);
  void sysexReceived(const uint8_t *data, unsigned len);

 private:
  struct Impl;
  std::unique_ptr<Impl> P;
};
