#include "program-name-validator.h"
#include <phatty.h>
#include <QtCore/QDebug>

QValidator::State ProgramNameValidator::validate(QString &input, int &pos) const {
  if (input.size() > 13) {
    return QValidator::Invalid;
  }
  for (QChar qch : input) {
    char ch = qch.toLatin1();
    if (ch == 0 || !strchr(PHATTY_CHARSET, ch))
      return QValidator::Invalid;
  }
  return QValidator::Acceptable;
}
