#include "app.h"

int main(int argc, char *argv[]) {
  QApplication::setApplicationName("Phatty Editor");
  QApplication::setOrganizationName("JPsoft");

  App app(argc, argv);
  app.init();
  int ret = app.exec();
  app.shutdown();
  return ret;
}
